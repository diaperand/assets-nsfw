# Credits

## Blindfold
	
Strap from included goggles model on ponies
	
## Ballgag
	
Belt buckle from BlueStream's Collar for Ponies model: [LINK](https://ask-bluestream.tumblr.com)
	
## Enhanced Pony models

http://juicedane.deviantart.com/
	
## Horse Cocks

 Both originally from BlueStream: [LINK](https://ask-bluestream.tumblr.com)

If I missed anything or anyone, feel free to suggest a PR and I'll add it in.
